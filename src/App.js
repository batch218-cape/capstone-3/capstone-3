import { useEffect, useState } from 'react';

import {UserProvider} from './UserContext';

import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'

import './App.css';

import AppNavbar from  './components/AppNavbar'; 
import ProductView from './components/ProductView';
import Promobar from './components/Promobar';
import Subscribe from './components/Subscribe';
import Footer from './components/Footer';

import Home from './pages/Home';
import Products from './pages/Products';
import Cart from './pages/account/Cart';
import Details from './pages/account/Details';
import Register from './pages/account/Register';
import Login from './pages/account/Login';
import Logout from './pages/account/Logout';

import Users from './pages/admin/Users';
import Inventory from './pages/admin/Inventory';

import ScrollToTop from './components/ScrollToTop';
import Error from './pages/Error';

function App() {
  const [user, setUser] = useState({
    id: null,
    firstname: null,
    lastname: null,
    email: null,
    mobile: null,
    joinDate: null,
    productsBought: null,
    cartTotal: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear()
  }

  const updateUserInfo = () => {
    fetch(`${process.env.REACT_APP_API_URL}/user/${localStorage.getItem('id')}`, {
      headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
        if(typeof data.user._id !== "undefined") {
          const timestamp = Date.parse(data.user.createdOn)
          const date = new Date(timestamp)
          const joinDate = `${date.toLocaleString('en-US', { month: 'long' })} ${date.getDate()}, ${date.getFullYear()} ${String(date.getHours()).padStart(2, '0')}:${String(date.getMinutes()).padStart(2, '0')}`

            setUser({
                id: data.user._id,
                firstname: data.user.firstname,
                lastname: data.user.lastname,
                email: data.user.email,
                mobile: data.user.mobile,
                productsBought: data.user.productsBought,
                cartTotal: data.user.cart.reduce((total, current) => total + current.quantity, 0),
                isAdmin: data.user.isAdmin,
                joinDate
            })
        } 
        else { 
            setUser({
                id: null,
                firstname: null,
                lastname: null,
                email: null,
                mobile: null,
                joinDate: null,
                productsBought: null,
                cartTotal: null,
                isAdmin: null
            })
        }
    })
  }

  useEffect(() => {
    updateUserInfo();
  }, []);

  return (
    <>
      <UserProvider value={{user, setUser, unsetUser, updateUserInfo}}>
        <Router>
          <ScrollToTop />
          <Promobar />
          <AppNavbar />
          <div className='my-container'>
            <Routes>
              <Route path="/" element={<Home/>} />
              <Route path="/collections" element={<Products/>} />
              <Route path="/collections/:collectionsHandle" element={<Products/>} />

              <Route path="/products/:productHandle" element={<ProductView/>} />

              <Route path="/account" element={<Details/>} />
              <Route path="/account/cart" element={<Cart/>} />

              <Route path="/account/register" element={<Register/>} />
              <Route path="/account/login" element={<Login/>} />
              <Route path="/account/logout" element={<Logout/>} />

              {/* Admin only */}
              <Route path="/admin/users" element={<Users/>} />
              <Route path="/admin/inventory" element={<Inventory/>} />

              <Route path="/error" element={<Error />} />
              <Route path="/*" element={<Error />} />
            </Routes>
          </div>
          <Subscribe />
          <Footer />
        </Router>
      </UserProvider>
    </>
  );
}

export default App;

import { useState } from "react";

export default function Subscribe() {

    const [ submitted, setSubmiited ] = useState(false)

    const subscribe = (e) => {
        e.preventDefault()
        document.getElementById('subscribe__input').classList.add('disabled')
        document.getElementById('subscribe__submit').classList.add('disabled')
        document.getElementById('subscribe__submit').innerHTML = 'subscribed'
        setSubmiited(true)

        setTimeout(() => {
            document.getElementById('subscribe__input').classList.remove('disabled')
            document.getElementById('subscribe__input').value = ''
            document.getElementById('subscribe__submit').classList.remove('disabled')
            document.getElementById('subscribe__submit').innerHTML = 'subscribe'
            setSubmiited(false)
        }, 1500)
    }

    return (
        <>
            <div className="subscribe">
                <p className="subscibe__heading">SUBSCRIBE</p>
                <p className="subscibe__subheading">RECEIVE UPDATES ON OUR LATEST NEWS & OFFERS.</p>
                <form onSubmit={ (e) => subscribe(e) } className="subscribe__form s-input">
                    <input id='subscribe__input' className='subscribe__input' type='email' placeholder='EMAIL ADDRESS' required disabled={submitted} />
                    <button id='subscribe__submit' className='subscribe__button s-button' type='submit' disabled={submitted}>subscribe</button>
                </form>
            </div>
        </>
    );
  }
  
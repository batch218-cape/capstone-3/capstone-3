export default function Promobar() {

  return (
    <>
        <div className="promobar">
            <p>For educational purposes only. <a href="https://github.com/alyssacape" target="_blank" rel="noreferrer">@alyssacape</a></p>
        </div>
    </>
  );
}

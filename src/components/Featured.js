
import { NavLink, Link } from 'react-router-dom';

export default function Featured({data}) {
    return (
        <>
            <div className='featured-products'>
                <div className='featured-products-left'>
                    <Link 
                        className='featured-product-big' as={NavLink} 
                        to={data[0].link} style={{backgroundImage: 'url("' + data[0].image + '")'}}
                    >
                        <div className='featured-product-backdrop'></div>
                        <p className='featured-product-cta'>SHOP NOW</p>
                    </Link>
                </div>
                <div className='featured-products-right'>
                    <Link 
                        className='featured-product-small' as={NavLink} 
                        to={data[1].link} style={{backgroundImage: 'url("' + data[1].image + '")'}}
                    >
                        <div className='featured-product-backdrop'></div>
                        <p className='featured-product-cta'>SHOP NOW</p>
                    </Link>

                    <Link 
                        className='featured-product-small' as={NavLink} 
                        to={data[2].link} style={{backgroundImage: 'url("' + data[2].image + '")'}}
                    >
                        <div className='featured-product-backdrop'></div>
                        <p className='featured-product-cta'>SHOP NOW</p>
                    </Link>

                    <Link 
                        className='featured-product-small' as={NavLink} 
                        to={data[3].link} style={{backgroundImage: 'url("' + data[3].image + '")'}}
                    >
                        <div className='featured-product-backdrop'></div>
                        <p className='featured-product-cta'>SHOP NOW</p>
                    </Link>

                    <Link 
                        className='featured-product-small' as={NavLink} 
                        to={data[4].link} style={{backgroundImage: 'url("' + data[4].image + '")'}}
                    >
                        <div className='featured-product-backdrop'></div>
                        <p className='featured-product-cta'>SHOP NOW</p>
                    </Link>

                    <Link 
                        className='featured-product-small' as={NavLink} 
                        to={data[5].link} style={{backgroundImage: 'url("' + data[5].image + '")'}}
                    >
                        <div className='featured-product-backdrop'></div>
                        <p className='featured-product-cta'>SHOP NOW</p>
                    </Link>

                    <Link 
                        className='featured-product-small' as={NavLink} 
                        to={data[6].link} style={{backgroundImage: 'url("' + data[6].image + '")'}}
                    >
                        <div className='featured-product-backdrop'></div>
                        <p className='featured-product-cta'>SHOP NOW</p>
                    </Link>
                </div>
            </div>
        </>
    )
}

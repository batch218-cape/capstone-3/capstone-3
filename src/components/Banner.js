
import { NavLink, Link } from 'react-router-dom';

export default function Banner({data}) {

    const { link, image, image_mobile, heading, subheading, align} = data;

    return (
        <>
            <Link className='homepage-banner desktop-only' as={NavLink} to={link} style={{backgroundImage: 'url("' + image + '")'}}>
                <div className={`homepage-banner-header homepage-banner-header--${align}`}>
                    <h1 className='homepage-banner-heading s-heading'>{ heading }</h1>
                    <p className='homepage-banner-subheading'>{ subheading }</p>
                </div>
            </Link>

            <Link className='homepage-banner mobile-only' as={NavLink} to={link} style={{backgroundImage: 'url("' + image_mobile + '")'}}>
                <div className={`homepage-banner-header homepage-banner-header--${align}`}>
                    <h1 className='homepage-banner-heading s-heading'>{ heading }</h1>
                    <p className='homepage-banner-subheading'>{ subheading }</p>
                </div>
            </Link>
        </>
    )
}


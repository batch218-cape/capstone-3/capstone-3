import {useContext} from 'react';
import {Link, NavLink} from 'react-router-dom'

import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import UserContext from '../UserContext';

export default function AppNavbar() {
  const { user } = useContext(UserContext);

  return (
    <>
      {/* DESKTOP ONLY */}
      <Navbar className='desktop-only' expand="lg">

        <div className='navbar-group navbar-group--left'>
          <div className='nav-link-wrapper'>
            <Nav.Link as={NavLink} to="/">Home</Nav.Link>
          </div>
          <div className='nav-link-wrapper'>
            <Nav.Link as={NavLink} to="/collections/all">Products</Nav.Link>
          </div>
        </div>
    
        <div className='navbar-group'>
          <Navbar.Brand as={Link} to="/">
            <img src="https://drive.google.com/uc?export=view&id=13hFFlFsq8E5AOJlx61bOMqtEDhaG1gZI" alt="Avery Logo" className="navbar__logo" />
          </Navbar.Brand>
        </div>
          
        <div className='navbar-group navbar-group--right'>
          { 
            (user.isAdmin === true && user.id !== null) &&
            <>
              <div className='nav-link-wrapper'>
                <Nav.Link as={NavLink} to="/admin/users">Users</Nav.Link>
              </div>
              <div className='nav-link-wrapper'>
                <Nav.Link as={NavLink} to="/admin/inventory">Inventory</Nav.Link>
              </div>
              <div className='nav-link-wrapper'>
                <Nav.Link as={NavLink} to="/account/logout">Logout</Nav.Link>
              </div>
            </>
          }

          { 
            (user.isAdmin === false && user.id !== null) &&
            <>
              <div className='nav-link-wrapper'>
                <Nav.Link as={NavLink} to="/account/cart">
                  Cart
                  {
                    (user.cartTotal > 0) && 
                    <span className='navbar-cart-count'>
                        { user.cartTotal <= 99 ? user.cartTotal : '99'}
                    </span>
                  }
                </Nav.Link>
              </div>
              <div className='nav-link-wrapper'>
                <Nav.Link as={NavLink} to="/account">Account</Nav.Link>
              </div>
              <div className='nav-link-wrapper'>
                <Nav.Link as={NavLink} to="/account/logout">Logout</Nav.Link>
              </div>
            </>
          }

          { 
            (user.id === null) &&
            <>
              <div className='nav-link-wrapper'>
                <Nav.Link as={NavLink} to="/account/login">Login</Nav.Link>
              </div>
              <div className='nav-link-wrapper'>
                <Nav.Link as={NavLink} to="/account/register">Register</Nav.Link>
              </div>
            </>
          }
        </div>
      </Navbar>
      

      {/* MOBILE ONLY */}
      <Navbar className='mobile-only' expand="lg">
          <Navbar.Brand as={Link} to="/">
            <img src="https://drive.google.com/uc?export=view&id=13hFFlFsq8E5AOJlx61bOMqtEDhaG1gZI" alt="Avery Logo" className="navbar__logo" />
          </Navbar.Brand>

          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ml-auto">
              <Nav.Link as={NavLink} to="/">Home</Nav.Link>
              <Nav.Link as={NavLink} to="/collections/all">Products</Nav.Link>

              { 
                (user.isAdmin === true && user.id !== null) &&
                <>
                  <Nav.Link as={NavLink} to="/admin/users">Users</Nav.Link>
                  <Nav.Link as={NavLink} to="/admin/inventory">Inventory</Nav.Link>
                  <Nav.Link as={NavLink} to="/account/logout">Logout</Nav.Link>
                </>
              }

              { 
                (user.isAdmin === false && user.id !== null) &&
                <>
                  <Nav.Link as={NavLink} to="/account/cart">
                    Cart
                    {
                      (user.cartTotal > 0) && 
                      <span className='navbar-cart-count'>
                          { user.cartTotal <= 99 ? user.cartTotal : '99'}
                      </span>
                    }
                  </Nav.Link>
                  <Nav.Link as={NavLink} to="/account">Account</Nav.Link>
                  <Nav.Link as={NavLink} to="/account/logout">Logout</Nav.Link>
                </>
              }

              {
                (user.id === null) &&
                <>
                  <Nav.Link as={NavLink} to="/account/login">Login</Nav.Link>
                  <Nav.Link as={NavLink} to="/account/register">Register</Nav.Link>
                </>
              }
            </Nav>
          </Navbar.Collapse>
      </Navbar>
    </>
  );
}


/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState, useContext } from 'react';
import { Rating } from 'react-simple-star-rating';

import UserContext from '../UserContext';

import { useNavigate, useParams } from 'react-router-dom';

export default function ProductView() {

    const { user, updateUserInfo } = useContext(UserContext)
    const [ product, setProduct ] = useState('');
    const [ gallery, setGallery ] = useState('');
    const [ galleryMobile, setGalleryMobile ] = useState('');
    const [ addQuantity, setAddQuantity ] = useState(1);
    const [ allowAdd, setAllowAdd ] = useState(true);
    const [ userRating, setUserRating ] = useState(0);
    const [ ratingId, setRatingId ] = useState('');

    const { productHandle } = useParams();
    const navigate = useNavigate();
	
    const createGallery = (images) => {
        let image1 = 'https://drive.google.com/uc?export=view&id=17ijuBFmBOGEZjyS90gSG4C3feGAR9Czd';
        let image2 = 'https://drive.google.com/uc?export=view&id=10SCYogmv3QU2rvqF7lG1GD6KD5AXyL7A';
        let newImages = [];
        if (typeof images !== 'undefined') {
            newImages = images.split(',')

            if (newImages.length > 0) {
                image1 = newImages[0]
            }

            if (newImages.length > 1) {
                image2 = newImages[1]
            }
        }

        if (newImages.length === 0) {
            newImages.push(image1)
            newImages.push(image2)
        } else if (newImages.length === 1) {
            newImages.push(image2)
        }

        const tempGallery = newImages.map(image => {
            return(
                <div className='product-gallery-image-wrapper'>
                    <img className='product-gallery-image' src={image} alt="" />
                </div>
            )
        })
        setGallery(tempGallery);
    }

    const createGalleryMobile = (images) => {
        let image1 = 'https://drive.google.com/uc?export=view&id=17ijuBFmBOGEZjyS90gSG4C3feGAR9Czd';
        let image2 = 'https://drive.google.com/uc?export=view&id=10SCYogmv3QU2rvqF7lG1GD6KD5AXyL7A';
        let newImages = [];
        if (typeof images !== 'undefined') {
            newImages = images.split(',')

            if (newImages.length > 0) {
                image1 = newImages[0]
            }

            if (newImages.length > 1) {
                image2 = newImages[1]
            }
        }

        if (newImages.length === 0) {
            newImages.push(image1)
            newImages.push(image2)
        } else if (newImages.length === 1) {
            newImages.push(image2)
        }

        const tempGallery = newImages.slice(0, 2).map(image => {
            return(
                <div className='product-gallery-image-wrapper'>
                    <img className='product-gallery-image' src={image} alt="" />
                </div>
            )
        })
        setGalleryMobile(tempGallery);
    }

    const addToCart = () => {
        if (product && addQuantity >= 1) {
            setAllowAdd(false)
            fetch(`${process.env.REACT_APP_API_URL}/user/${user.id}/cart/add`, {
                method: 'PATCH',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${ localStorage.getItem('token') }`
                },
                body: JSON.stringify({
                    productId: product._id,
                    quantity: addQuantity
                })
            })
            .then(res => res.json())
            .then(data => {
                document.getElementById(`product-info-cart-add`).classList.add('done')

                if(data.status === 200) {
                    document.getElementById(`product-info-cart-add`).innerHTML = 'ADDED'
                    updateUserInfo()
                } else {
                    document.getElementById(`product-info-cart-add`).innerHTML = 'ERROR'
                }

                setTimeout(() => {
                    document.getElementById(`product-info-cart-add`).classList.remove('done')
                    document.getElementById(`product-info-cart-add`).innerHTML = 'ADD TO CART'
                    setAllowAdd(true)
                }, 1200)
            })
        }
    }

    const rateProduct = (newRating) => {
        if (ratingId !== '') {
            fetch(`${process.env.REACT_APP_API_URL}/review/${ratingId}/update`, {
                method: 'PATCH',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${ localStorage.getItem('token') }`
                },
                body: JSON.stringify({
                    rating: newRating
                })
            })
            .then(res => res.json())
            .then(data => {
                setUserRating(newRating)
                fetchData()
            })
        } else {
            fetch(`${process.env.REACT_APP_API_URL}/review/create`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${ localStorage.getItem('token') }`
                },
                body: JSON.stringify({
                    productId: product._id,
                    rating: newRating
                })
            })
            .then(res => res.json())
            .then(data => {
                setUserRating(newRating)
                fetchData()
            })
        }
    }

    const fetchProductData = () => {
        return fetch(`${process.env.REACT_APP_API_URL}/product/by-handle/${productHandle}`)
        .then(res => res.json())
        .then(data => {
            if(data.product === null) {
                navigate('/error')
            }

            setProduct(data.product)
            createGallery(data.product.images)
            createGalleryMobile(data.product.images)
            return data.product
        });
    }

    const fetchUserRating = (userId, productId) => {
        fetch(`${process.env.REACT_APP_API_URL}/review//user/${userId}/product/${productId}`)
        .then(res => res.json())
        .then(data => {
            if (data.review !== null) {
                setUserRating(data.review.rating)
                setRatingId(data.review._id)
            }
        });
    }

    const fetchData = async () => {
        const productData = await fetchProductData()
        await fetchUserRating(localStorage.getItem('id'), productData._id)
    }

    useEffect(() => {
        fetchData();
    }, [])

	return (
        <>
            <div className='product'>
                <div className='product-gallery'>
                    <div className='product-gallery-desktop desktop-only'>
                        {gallery}
                    </div>
                    <div className='product-gallery-mobile mobile-only'>
                        {galleryMobile}
                    </div>
                </div>
                <div className='product-info'>
                    <div className='product-info-wrapper'>
                        <p className='product-info-name'>{product?.name}</p>
                        <p className='product-info-category'>{product?.category}</p>
                        <p className='product-info-rating'>
                            <Rating
                                initialValue={product?.rating ?? 0}
                                fillColor='#151515'
                                size={20}
                                allowFraction
                                transition
                                readonly
                            />
                        </p>
                        <p className='product-info-price'>USD ${product?.price?.toFixed(2)}</p>
                        <p className='product-info-description'>{product?.description}</p>
                        { 
                            (user.id === null) &&
                            <>
                                <div className='product-info-cart-input s-input'>
                                    <input className='product-info-cart-input-field' type='number' value='0' />
                                    <button className='product-info-cart-add s-button s-button--disabled' disabled>LOGIN TO ADD</button>
                                </div>
                            </>
                        }

                        { 
                            (user.isAdmin === false && user.id !== null) &&
                            <>
                                <div className='product-info-cart-input s-input'>
                                    <input 
                                        className='product-info-cart-input-field' 
                                        type='number' value={addQuantity} disabled={!allowAdd}
                                        onChange= { e => { if (e.target.value > 0) setAddQuantity(e.target.value)} }
                                    />
                                    <button id='product-info-cart-add' className='product-info-cart-add s-button' onClick={() => addToCart()} disabled={!allowAdd}>ADD TO CART</button>
                                </div>
                            </>
                        }

                        {
                            (user.isAdmin === false && user.id !== null && user?.productsBought?.includes(product?._id)) &&
                            <>
                                <div className='product-info-feedback'>
                                    <p className='product-info-feedback__title s-heading'>
                                        {
                                            (userRating > 0) ?
                                                <>
                                                    You rated this product
                                                </>
                                            :
                                                <>
                                                    Rate your purchase
                                                </>
                                        }
                                    </p>
                                    <Rating 
                                        onClick={rateProduct}
                                        initialValue={userRating}
                                        onPointerLeave={() => setUserRating(userRating)}
                                        fillColor='#151515'
                                        allowHover={false}
                                        disableFillHover
                                        transition
                                        size={30}
                                    />
                                </div>
                            </>
                        }
                    </div>
                </div>
            </div>
        </>
	)
}
/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from 'react';
import { Link, NavLink } from 'react-router-dom';

export default function Highlights() {
	const [grid, setGrid] = useState([]);

	const createGrid = (products) => {
        const tempGrid = products.map(product => {
            let image1 = 'https://drive.google.com/uc?export=view&id=17ijuBFmBOGEZjyS90gSG4C3feGAR9Czd';
            let image2 = 'https://drive.google.com/uc?export=view&id=10SCYogmv3QU2rvqF7lG1GD6KD5AXyL7A';
            if (typeof product.images !== 'undefined') {
                const images = product.images.split(',')

                if (images.length > 0) {
                    image1 = images[0]
                }

                if (images.length > 1) {
                    image2 = images[1]
                }
            }

            return(
                <div className='product-item' key={product._id}>
                    <Link as={NavLink} to={`/products/${product.handle}`} className='product-item-link'>
                        <div className='product-item-image-wrapper'>
                            <img className='product-item-image' src={image1} alt={product.name} />
                            <img className='product-item-image product-item-image--hover' src={image2} alt={product.name} />
                        </div>
                        <div className='product-item-info'>
                            <p className='product-item-title'>{product.name}</p>
                            <p className='product-item-price'>${product.price.toFixed(2)}</p>
                            <p className='product-item-category'>{product.category}</p>
                        </div>
                    </Link>
                </div>
            )
        })
        setGrid(tempGrid);
    }

    const fetchData = () => {
        fetch(`${process.env.REACT_APP_API_URL}/product/featured`)
        .then(res => res.json())
        .then(data => {
            createGrid(data.products)
        });
    }

    useEffect(() => {
        fetchData();
    }, [])

	return (
		<>
			{
				(grid.length) > 0 &&
					<div className='highlights-grid'>
						{grid}
					</div>
			}
		</>
	)
}
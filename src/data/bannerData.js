const bannerData = [
    {
        id: "banner001",
        heading: "MAMOUNIA",
        subheading: "Postcards from Morocco. Discover our latest collection.",
        button: "SHOP NOW",
        link: "/collections/all",
        image: "https://drive.google.com/uc?export=view&id=1f82erbyn_J0wUVwZFP0CpamaxbUh5ypk",
    }
]
export default bannerData;
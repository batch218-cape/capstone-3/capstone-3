import Banner from  '../components/Banner'; 
import Featured from '../components/Featured';
import Highlights from  '../components/Highlights'; 
 

export default function Home() {

    const banner = {
        link: '/collections/all',
        image: "https://drive.google.com/uc?export=view&id=1X9T8pauQXquUGghTy-jWR-vmLKVWefK7",
        image_mobile: "https://drive.google.com/uc?export=view&id=1Usp5AClD5CvmtOmGCbCjHBCG0rjiMa69",
        heading: 'DRESS TO IMPRESS',
        subheading: 'SHOP NOW',
        align: 'right'
    }

    const banner2 = {
        link: '/collections/all',
        image: "https://drive.google.com/uc?export=view&id=10vDbfThD-4UNwn73eYBjgPqQFSdATjtL",
        image_mobile: "https://drive.google.com/uc?export=view&id=18XN34lbCbUH3Cd7pcNL8yBC8y4wvq4I-",
        heading: 'PARTY WITH STYLE',
        subheading: 'SHOP NOW',
        align: 'left'
    }

    const featured = [
        {
            link: '/products/tilda-v-neck-knit-maxi-dress-ivory',
            image: 'https://drive.google.com/uc?export=view&id=1T-WNGC4Ge92EVoBnGhvufL1OvOxoZCNp'
        },
        {
            link: '/products/clover-mini-dress-vanilla-bean',
            image: 'https://drive.google.com/uc?export=view&id=1I-qC6br0rvlPYtx9MYoZwBY_ipHfYXUF'
        },
        {
            link: '/products/coyote-midi-dress-camel-ivory',
            image: 'https://drive.google.com/uc?export=view&id=1823sHGy5J8DQClBlv9qCbisTLsn0RIMM'
        },
        {
            link: '/products/dilkon-maxi-dress-dusk-blue',
            image: 'https://drive.google.com/uc?export=view&id=1M7XCCteUo9xHoJ32Rp79FeJtji-w_aoL'
        },
        {
            link: '/products/effie-knit-key-maxi-dress-ivory',
            image: 'https://drive.google.com/uc?export=view&id=1oPaB6BAKrgAxOw7uJWleAxPMXSnHnrvp'
        },
        {
            link: '/products/indi-maxi-dress-sand-ivory',
            image: 'https://drive.google.com/uc?export=view&id=1A_rXh0sf1IBQIs2lvu2gD776DVc89E7m'
        },
        {
            link: '/products/siren-midi-dress-multi',
            image: 'https://drive.google.com/uc?export=view&id=11EOgdgrQ1MSBmplw5TWVo-Z3f7jQ4eqA'
        },
    ]

    return (
        <>
            <Banner data={banner}/>
            <Highlights />
            <Banner data={banner2}/>
            <Featured data={featured}/>
        </>
    )
}
/* eslint-disable react-hooks/exhaustive-deps */
import {useEffect, useState} from 'react';
import { useParams, Link, NavLink, Navigate } from 'react-router-dom';
import { Rating } from 'react-simple-star-rating';

export default function Courses() {
    const [grid, setGrid] = useState([]);

    const validHandles = ['all', 'mini', 'midi', 'maxi'];
    const { collectionsHandle } = useParams();

    const createGrid = (products) => {
        const tempGrid = products.map(product => {
            let image1 = 'https://drive.google.com/uc?export=view&id=17ijuBFmBOGEZjyS90gSG4C3feGAR9Czd';
            let image2 = 'https://drive.google.com/uc?export=view&id=10SCYogmv3QU2rvqF7lG1GD6KD5AXyL7A';
            if (typeof product.images !== 'undefined') {
                const images = product.images.split(',')

                if (images.length > 0) {
                    image1 = images[0]
                }

                if (images.length > 1) {
                    image2 = images[1]
                }
            }

            return(
                <div className='product-item' key={product._id}>
                    <Link as={NavLink} to={`/products/${product.handle}`} className='product-item-link'>
                        <div className='product-item-image-wrapper'>
                            <img className='product-item-image' src={image1} alt={product.name} />
                            <img className='product-item-image product-item-image--hover' src={image2} alt={product.name} />
                        </div>
                        <div className='product-item-info'>
                            <p className='product-item-title'>{product.name}</p>
                            <p className='product-item-price'>${product.price.toFixed(2)}</p>
                            <p className='product-item-category'>{product.category}</p>
                            <p className='product-item-rating'>
                                <Rating
                                    initialValue={product?.rating ?? 0}
                                    fillColor='#151515'
                                    size={16}
                                    allowFraction
                                    readonly
                                />
                            </p>
                        </div>
                    </Link>
                </div>
            )
        })
        setGrid(tempGrid);
    }

    const fetchData = (collectionsHandle) => {
        fetch(`${process.env.REACT_APP_API_URL}/product/active/by-handle/${collectionsHandle ?? 'all'}`)
        .then(res => res.json())
        .then(data => {
            createGrid(data.products)
        });
    }

    useEffect(() => {
        fetchData(collectionsHandle);
    }, [collectionsHandle])

    return(
        (validHandles.includes(collectionsHandle) === false) ?
            <Navigate to='/error'/>
        :
        <>
            <div className='products'>
                <div className='products-navigation'>
                    <h1 className='products-heading s-heading'>SHOP {collectionsHandle ?? 'ALL'}</h1>
                    <ul className='products-categories'>
                        <li className='products-category'><Link as={NavLink} to='/collections/all'>ALL</Link></li>
                        <li className='products-category'><Link as={NavLink} to='/collections/mini'>MINI</Link></li>
                        <li className='products-category'><Link as={NavLink} to='/collections/midi'>MIDI</Link></li>
                        <li className='products-category'><Link as={NavLink} to='/collections/maxi'>MAXI</Link></li>
                    </ul>
                </div>
                <div className='products-grid'>
                    { 
                        (grid.length > 0) ?
                            <>{grid}</>
                        :
                            <>
                                <div className='products-grid-empty'>
                                    <p className='products-grid-empty__heading'>
                                        No products for this category
                                    </p>
                                    <p className='products-grid-empty__subheading'>
                                        Click <Link as={NavLink} to='/collections/all'>here</Link> to check out all our products
                                    </p>
                                </div>
                            </>
                    }
                </div>
            </div>
        </>
    )
}
/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import { Modal, Table, Form } from 'react-bootstrap';
import UserContext from '../../UserContext';

export default function Courses() {
    const { user } = useContext(UserContext);

    const [table, setTable] = useState([]);
    const [productId, setProductId] = useState('');
    const [name, setName] = useState('');
    const [handle, setHandle] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState('');
    const [images, setImages] = useState('');
    const [previews, setPreviews] = useState('');
    const [category, setCategory] = useState('');
    const [modalActive, setModalActive] = useState(false);

    const fieldsAsString = [
        'name',
        'handle',
        'description',
        'price',
        'images',
        'category',
    ]

    const resetForm = () => {
        setProductId('');
        setName('');
        setHandle('');
        setDescription('')
        setPrice('')
        setImages('')
        setCategory('')
        setPreviews(null)
    }

    const openModal = (id='') => {
        if(id !== '') {
            fetch(`${process.env.REACT_APP_API_URL}/product/${id}`)
            .then(res => res.json())
            .then(data => {
                setProductId(data.product._id);
                setName(data.product.name);
                setHandle(data.product.handle);
                setDescription(data.product.description)
                setPrice(data.product.price)
                setImages(data.product.images)
                setCategory(data.product.category)
            })
        } else {
            resetForm()
        }

        setModalActive(true);
    }

    const closeModal = () => {
        setModalActive(false);
        resetForm();
    }

    const submitForm = (e, id='') => {
        e.preventDefault();
        document.getElementById(`inventory-error`).classList.add('hidden')

        if(id !== '') {
            fetch(`${process.env.REACT_APP_API_URL}/product/${id}/update`, {
                method: 'PATCH',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${ localStorage.getItem('token') }`
                },
                body: JSON.stringify({
                    name: name,
                    handle: handle,
                    description: description,
                    price: price,
                    images: images,
                    category: category
                })
            })
            .then(res => res.json())
            .then(data => {
                if(data.hasOwnProperty('errors')) {
                    document.getElementById(`inventory-error`).classList.add('hidden')
                    for (const key in data.errors) {
                        if (fieldsAsString.includes(key)) {
                            document.getElementById(`${key}-error`).innerHTML = data.errors[key].message
                            document.getElementById(`${key}-error`).classList.remove('hidden')
                        } else {
                            document.getElementById(`${key}-error`).classList.add('hidden')
                        }
                    }
                } else {
                    for (const index in fieldsAsString) {
                        if (document.getElementById(`${fieldsAsString[index]}-error`)) {
                            document.getElementById(`${fieldsAsString[index]}-error`).classList.add('hidden')
                        }
                    }

                    if (data.message) {
                        document.getElementById(`inventory-error`).innerHTML = data.message
                        document.getElementById(`inventory-error`).classList.remove('hidden')
                    } else {
                        document.getElementById(`inventory-error`).classList.add('hidden')
                    }

                    if (data?.status === 200) {
                        fetchData();
                    }
                }
            })

        } else {
            fetch(`${process.env.REACT_APP_API_URL}/product/create`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${ localStorage.getItem('token') }`
                },
                body: JSON.stringify({
                    name: name,
                    handle: handle,
                    description: description,
                    price: price,
                    images: images,
                    category: category
                })
            })
            .then(res => res.json())
            .then(data => {
                if(data.hasOwnProperty('errors')) {
                    document.getElementById(`inventory-error`).classList.add('hidden')
                    for (const key in data.errors) {
                        if (fieldsAsString.includes(key)) {
                            document.getElementById(`${key}-error`).innerHTML = data.errors[key].message
                            document.getElementById(`${key}-error`).classList.remove('hidden')
                        } else {
                            document.getElementById(`${key}-error`).classList.add('hidden')
                        }
                    }
                } else {
                    for (const index in fieldsAsString) {
                        if (document.getElementById(`${fieldsAsString[index]}-error`)) {
                            document.getElementById(`${fieldsAsString[index]}-error`).classList.add('hidden')
                        }
                    }

                    if (data.message) {
                        document.getElementById(`inventory-error`).innerHTML = data.message
                        document.getElementById(`inventory-error`).classList.remove('hidden')
                    } else {
                        document.getElementById(`inventory-error`).classList.add('hidden')
                    }

                    if (data?.status === 201) {
                        resetForm();
                        fetchData();
                    }
                }
            })
        }
    }


    const setActive = (id, setAs) => {
        fetch(`${process.env.REACT_APP_API_URL}/product/${id}/${setAs}`, {
            method: 'PATCH',
            headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			}
        })
        .then(res => res.json())
        .then(data => fetchData())

    }

    const createTable = (inventory) => {
        const tempTable = inventory.map(product => {
                return(
                    <tr key={product._id}>
                        <td width="auto">{product.name}</td>
                        <td width="auto">{product.handle}</td>
                        <td width="100px">{product.category}</td>
                        <td width="50px" className='text-center'>{product.rating.toFixed(1)}</td>
                        <td width="200px" className='text-right'>{product.price.toFixed(2)}</td>
                        <td width="50px" className='text-center'>
                            { 
                                product.isActive === true ?
                                    <button className='s-button' onClick={() => setActive(product._id, 'archive')}>Y</button>
                                :
                                    <button className='s-button s-button--secondary' onClick={() => setActive(product._id, 'unarchive')}>N</button>
                            }
                        </td>
                        <td width="100px" className='text-center'>
                            <button className='s-button' onClick={() => openModal(product._id)}>Update</button>
                        </td>
                    </tr>
                )
            })
        setTable(tempTable);
    }

    const fetchData = (empty=false) => {
        if(empty === true) {
            setTable([])
        }

        fetch(`${process.env.REACT_APP_API_URL}/product`)
        .then(res => res.json())
        .then(data => {
            createTable(data.products)
        });
    }

    useEffect(() => {
        fetchData();
    }, [])

    useEffect(() => {
        if(typeof images !== 'undefined') {
            const splitImages = images.split(',')
            const tempPreviews = splitImages.map(preview => {
                return (
                    <>
                        <div className='thumbnail-preview'>
                            <img src={preview} alt={preview} />
                        </div>
                    </>
                )
            })
            setPreviews(tempPreviews);
        } else {
            setPreviews(null)
        }
    }, [images])

    return(
        (user.id === null || user.isAdmin === false) ?
            <Navigate to='/'/>
        :
        <>
            <div className='inventory'>
                <div className='inventory__heading'>
                    <h1 className='inventory__title s-heading'>Inventory</h1>
                    <div className='inventory__actions'>
                        <button className='s-button' onClick={() => openModal()}>New Product</button>
                        <button className='s-button s-button--secondary' onClick={() => fetchData(true)}>Reload</button>
                    </div>
                </div>
                <Table striped bordered responsive>
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Handle</th>
                            <th>Category</th>
                            <th className='text-center'>Rating</th>
                            <th>Price (USD)</th>
                            <th className='text-center'>Active?</th>
                            <th className='text-center'>Actions</th>
                        </tr>
                    </thead>
                    <tbody><>{table}</></tbody>
                </Table>

                <Modal show={modalActive} onHide={() => closeModal()} centered>
                    <Form onSubmit={e => submitForm(e, productId)} noValidate>
                        <Modal.Header>
                            <Modal.Title className='s-heading'>
                                { (productId !== '') ? <>Update Product</> : <>New Product</> }
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <div id='inventory-error' className='s-form-error s-form-error--secondary hidden'>Form contains errors</div>
                            <div className='s-input'>
                                <label htmlFor="name">Name</label>
                                <input
                                    id="name"
                                    name="name"
                                    type="text"
                                    value={name}
                                    onChange= {e => setName(e.target.value)}
                                    required
                                />
                                <div id="name-error" className='s-input-error hidden'>
                                    Please enter the product's name
                                </div>
                            </div>

                            <div className='s-input'>
                                <label htmlFor="name">Handle</label>
                                <input
                                    id="handle"
                                    name="handle"
                                    type="text"
                                    value={handle}
                                    onChange= {e => setHandle(e.target.value)}
                                    required
                                />
                                <div id="handle-error" className='s-input-error hidden'>
                                    Please enter the product's handle
                                </div>
                            </div>

                            <div className='s-input'>
                                <label htmlFor="name">Description</label>
                                <input
                                    id="description"
                                    name="description"
                                    type="text"
                                    value={description}
                                    onChange= {e => setDescription(e.target.value)}
                                    required
                                />
                                <div id="description-error" className='s-input-error hidden'>
                                    Please enter the product's description
                                </div>
                            </div>

                            <div className='s-input'>
                                <label htmlFor="name">Price (in usd)</label>
                                <input
                                    id="price"
                                    name="price"
                                    type="number"
                                    value={price}
                                    onChange= {e => setPrice(e.target.value)}
                                    required
                                />
                                <div id="price-error" className='s-input-error hidden'>
                                    Please enter the product's price
                                </div>
                            </div>

                            <div className='s-input'>
                                <label htmlFor="name">Category (mini, midi, maxi)</label>
                                <input
                                    id="category"
                                    name="category"
                                    type="text"
                                    value={category}
                                    onChange= {e => setCategory(e.target.value)}
                                    required
                                />
                                <div id="category-error" className='s-input-error hidden'>
                                    Please enter the product's category
                                </div>
                            </div>

                            <div className='s-input'>
                                <label htmlFor="name">Image Links (separate links with commas)</label>
                                <input
                                    id="images"
                                    name="images"
                                    type="text"
                                    value={images}
                                    onChange= {e => setImages(e.target.value)}
                                    required
                                />
                                <div id="images-error" className='s-input-error hidden'>
                                    Please enter the product's images
                                </div>
                            </div>

                            { (previews !== null) &&
                                <div className='s-input'>
                                    <label htmlFor="name">Image Previews</label>
                                    <div className='thumbnail-preview-container'>{previews}</div>
                                </div>
                            }
                        </Modal.Body>
                        <Modal.Footer>
                            <button className='s-button s-button--secondary' onClick={() => closeModal()}>Cancel</button>
                            <button className='s-button' type="submit">Submit</button>
                        </Modal.Footer>
                    </Form>
                </Modal>

            </div>
        </>
    )
}
/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import { Modal, Table, Form } from 'react-bootstrap';
import UserContext from '../../UserContext';

export default function Users() {
    const { user } = useContext(UserContext);

    const [userTable, setUserTable] = useState([]);
    const [ordersTable, setOrdersTable] = useState([]);
    const [ordersCount, setOrdersCount] = useState(0);

    const [userId, setUserId] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');

    const [modalPasswordActive, setModalPasswordActive] = useState(false);
    const [modalOrdersActive, setModalOrdersActive] = useState(false);

    const resetPasswordForm = (resetId=false) => {
        if (resetId === true) {
            setUserId('');
        }

        setPassword1('');
        setPassword2('');
    }

    const openPasswordModal = (id='') => {
        setUserId(id)
        setModalPasswordActive(true);
        resetPasswordForm()
    }

    const closePasswordModal = () => {
        setModalPasswordActive(false);
        resetPasswordForm(true);
    }

    const setPassword = (e) => {
        e.preventDefault()
        document.getElementById(`user-password-error`).classList.add('hidden')
        if (password1 === password2) {
            fetch(`${process.env.REACT_APP_API_URL}/user/${userId}/password`, {
                method: 'PATCH',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${ localStorage.getItem('token') }`
                },
                body: JSON.stringify({
                    password: password1
                })
            })
            .then(res => res.json())
            .then(data => {
                if (data.status === 200) {
                    resetPasswordForm()
                }

                const message = data.message
                document.getElementById(`user-password-error`).innerHTML = message
                document.getElementById(`user-password-error`).classList.remove('hidden')
            })
        } else {
            const message = `Please ensure that the Password and Confirm Password fields are the same`
            document.getElementById(`user-password-error`).innerHTML = message
            document.getElementById(`user-password-error`).classList.remove('hidden')
        }
    }

    const openOrdersModal = (id='') => {
        setOrdersTable([]);
        setModalOrdersActive(true);

        if (id !== '') {
            retrieveUserOrders(id)
        }
    }

    const closeOrdersModal = () => {
        setModalOrdersActive(false);
        setOrdersTable([]);
    }

    const createOrderItem = (products) => {
        const tempProducts = products.map(product => {
            return(
                <div className='account-order-item' key={product._id}>
                    <div className='account-order-item-image-wrapper'>
                        <img className='account-order-item-image' src={product.image} alt=''/>
                    </div>
                    <div className='account-order-item-info'>
                        <p className='account-order-item-name'>{product.name} ({product.quantity})</p>
                        <p className='account-order-item-price'>USD ${ (product.price * product.quantity).toFixed(2) }</p>
                    </div>
                </div>
            )
        })
        return tempProducts
    }

    const createOrdersData = (userOrders) => {
        const tempOrders = userOrders.map( (order) => {
            const orderItems = createOrderItem(order.products)
            const timestamp = Date.parse(order.createdOn)
            const date = new Date(timestamp)
            const formattedDate = `${date.toLocaleString('en-US', { month: 'long' })} ${date.getDate()}, ${date.getFullYear()} ${String(date.getHours()).padStart(2, '0')}:${String(date.getMinutes()).padStart(2, '0')}`
            return (
                <div id={`account-order-${order._id}`} className='account-order' key={order._id}>
                    <div className='account-order-header'>
                        <p className='s-heading account-order-heading'>Order ID: { order._id }</p>
                    </div>
                    <div className='account-order-info'>
                        <div className='account-order-item-info-group'>
                            <div className='s-input account-order-item-payment'>
                                <label htmlFor='account-order-item-payment'>PAYMENT METHOD</label>
                                <input id='account-order-item-payment' value={ order.paymentMethod } disabled />
                            </div>
                            <div className='s-input account-order-item-date'>
                                <label htmlFor='account-order-item-date'>ORDER DATE</label>
                                <input id='account-order-item-date' value={ formattedDate } disabled />
                            </div>
                        </div>
                        <div className='s-input account-order-item-address'>
                            <label htmlFor="account-order-item-address">DELIVERY ADDRESS</label>
                            <input id='account-order-item-address' value={order.deliveryAddress} disabled />
                        </div>
                    </div>
                    <div className='account-order-list'>
                        <div className='account-order-item-header'>
                            <p className='account-order-item-total'>TOTAL</p>
                            <p className='account-order-item-total-price'>USD ${order.totalPrice.toFixed(2)}</p>
                        </div>
                        { orderItems }
                    </div>
                </div>
            )
        })
        setOrdersTable(tempOrders);
    }

    const retrieveUserOrders = (id) => {
        fetch(`${process.env.REACT_APP_API_URL}/order/user/${id}`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
          })
          .then(res => res.json())
          .then(data => {
              setOrdersCount(0)

              if(data.count > 0) {
                createOrdersData(data.orders)
                setOrdersCount(data.count)
              }
          })
    }

    const setAdmin = (id, setAs) => {
        fetch(`${process.env.REACT_APP_API_URL}/user/${id}/status/${setAs}`, {
            method: 'PATCH',
            headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			}
        })
        .then(res => res.json())
        .then(data => fetchData())
    }

    const createUserTable = (inventory) => {
        const tempTable = inventory.map(currentUser => {
                return(
                    <tr key={currentUser._id}>
                        <td>{currentUser.lastname}, {currentUser.firstname}</td>
                        <td>{currentUser.email}</td>
                        <td>{currentUser.mobile}</td>
                        <td width='50px' className='text-center'>
                            {   
                                (currentUser._id !== user.id) ?
                                    <>
                                        { 
                                            currentUser.isAdmin === true ?
                                                <button className='s-button' onClick={() => setAdmin(currentUser._id, 'user')}>Y</button>
                                            :
                                                <button className='s-button s-button--secondary' onClick={() => setAdmin(currentUser._id, 'admin')}>N</button>
                                        }
                                    </>
                                :
                                    <>
                                        <button className='s-button s-button--disabled' disabled>Y</button>
                                    </>
                            }
                        </td>
                        <td width='350px' className='text-center'>
                            <div className='users__action-buttons'>
                                <button className='users__action-button s-button' onClick={() => openPasswordModal(currentUser._id)}>Update Password</button>
                                <button className='users__action-button s-button' onClick={() => openOrdersModal(currentUser._id)}>Order History</button>
                            </div>
                        </td>
                    </tr>
                )
            })
        setUserTable(tempTable);
    }

    const fetchData = (empty=false) => {
        if(empty === true) {
            setUserTable([])
        }

        fetch(`${process.env.REACT_APP_API_URL}/user`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            createUserTable(data.users)
        });
    }

    useEffect(() => {
        fetchData();
    }, [])

    return(
        (user.id === null || user.isAdmin === false) ?
            <Navigate to='/'/>
        :
        <>
            <div className='users'>
                <div className='users__heading'>
                    <h1 className='users__title s-heading'>Users</h1>
                    <div className='users__actions'>
                        <button className='s-button s-button--secondary' onClick={() => fetchData(true)}>Reload</button>
                    </div>
                </div>
                <Table striped bordered responsive>
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th className='text-center'>Admin?</th>
                            <th className='text-center'>Actions</th>
                        </tr>
                    </thead>
                    <tbody><>{userTable}</></tbody>
                </Table>
                <Modal show={modalOrdersActive} onHide={() => closeOrdersModal()} size="lg" centered>
                    <Modal.Header>
                        <Modal.Title className='s-heading'>
                            Order History ({ordersCount})
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        {ordersTable}
                        {
                            (ordersTable.length < 1) &&
                            <div className='account-order-list-empty'>
                                <p className='account-order-list-empty__heading'>
                                    No orders
                                </p>
                                <p className='account-order-list-empty__subheading'>
                                    User has not ordered anything yet
                                </p>
                            </div>
                        }
                    </Modal.Body>
                    <Modal.Footer>
                        <button className='s-button s-button--secondary' type='button' onClick={() => closeOrdersModal()}>Close</button>
                    </Modal.Footer>
                </Modal>
                <Modal show={modalPasswordActive} onHide={() => closePasswordModal()} centered>
                    <Form onSubmit={(e) => setPassword(e)} noValidate>
                        <Modal.Header>
                            <Modal.Title className='s-heading'>
                                Update Password
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <div id='user-password-error' className='s-form-error s-form-error--secondary hidden'>Form contains errors</div>
                            <div className='s-input'>
                                <label htmlFor="name">New Password</label>
                                <input
                                    id="name"
                                    name="name"
                                    type="password"
                                    value={password1}
                                    onChange= {e => setPassword1(e.target.value)}
                                    required
                                />
                                <div id="password-error" className='s-input-error hidden'>
                                    Please enter new password
                                </div>
                            </div>

                            <div className='s-input'>
                                <label htmlFor="name">Confirm Password</label>
                                <input
                                    id="handle"
                                    name="handle"
                                    type="password"
                                    value={password2}
                                    onChange= {e => setPassword2(e.target.value)}
                                    required
                                />
                                <div id="confirm-password-error" className='s-input-error hidden'>
                                    Plese enter the password again
                                </div>
                            </div>
                        </Modal.Body>
                        <Modal.Footer>
                            <button className='s-button s-button--secondary' type='button' onClick={() => closePasswordModal()}>Cancel</button>
                            <button className='s-button' type="submit">Submit</button>
                        </Modal.Footer>
                    </Form>
                </Modal>
            </div>
        </>
    )
}
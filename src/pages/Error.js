import { Link, NavLink } from 'react-router-dom'

export default function Error() {

    return (
        <>
            <div className='error'>
                <p className='s-heading error__heading'>
                    Page does not exist
                </p>
                <img className='error__image' src='https://drive.google.com/uc?export=view&id=18CNU2kIQqn90Lspndov_bIYT_feNxCLF' alt='' />
                <p className='s-heading-info error__subheading'>
                    Click <Link as={NavLink} to='/' className='s-link'>here</Link> to return to homepage
                </p>
            </div>
        </>
    )
}
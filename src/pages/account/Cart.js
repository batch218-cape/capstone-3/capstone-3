/* eslint-disable react-hooks/exhaustive-deps */
import { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate, Link, NavLink } from 'react-router-dom';
import UserContext from '../../UserContext';

export default function Cart(props) {

    const { user, updateUserInfo } = useContext(UserContext);
    const [ deliveryAddress, setDeliveryAddress ] = useState('');
    const [ myCart, setMyCart ] = useState([]);
    const [ allowSubmit, setAllowSubmit ] = useState(true);
    const [ total, setTotal ] = useState(0);

    const navigate = useNavigate();

    const fetchCartList = async (newCart) => {
        const tempCart = [];
        for (const newProduct of newCart) {
            const tempProduct = await fetch(`${process.env.REACT_APP_API_URL}/product/${newProduct.productId}`)
            .then(res => res.json())
            .then(data => {
                const image = (typeof data.product.images !== 'undefined') ? 
                                data.product.images.split(',')[0] : 
                                'https://drive.google.com/uc?export=view&id=10SCYogmv3QU2rvqF7lG1GD6KD5AXyL7A';
                const tempProduct = {
                    id: data.product._id,
                    name: data.product.name,
                    handle: data.product.handle,
                    description: data.product.description,
                    price: data.product.isActive ? data.product.price : 0,
                    total: data.product.isActive ? data.product.price * newProduct.quantity : 0,
                    quantity: newProduct.quantity,
                    isActive: String(data.product.isActive),
                    image
                }

                return tempProduct
            });

            tempCart.push(tempProduct)
        }
        return tempCart
    }

    const updateQuantity = (id, value) => {
        const newValue = value <= 0 ? 1 : value
        document.getElementById(`cart-item-quantity-${id}`).value = newValue
    }

    const updatePrice = (id) => {
        const price = document.getElementById(`cart-item-individual-price-${id}`).value
        const quantity = document.getElementById(`cart-item-quantity-${id}`).value
        const total = Number(price) * Number(quantity) 
        document.getElementById(`cart-item-price-${id}`).innerHTML = `USD $${total.toFixed(2)}`
    }

    const setQuantity = (id) => {
        setAllowSubmit(false)
        fetch(`${process.env.REACT_APP_API_URL}/user/${user.id}/cart/update`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            },
            body: JSON.stringify({
                productId: id,
                quantity: document.getElementById(`cart-item-quantity-${id}`).value
            })
        })
        .then(res => res.json())
        .then(data => {
            document.getElementById(`cart-item-quantity-button-${id}`).classList.add('done')

            if(data.status === 200) {
                document.getElementById(`cart-item-quantity-button-${id}`).innerHTML = 'UPDATED'
                retrieveUserCart(false)
                updateUserInfo()
                updatePrice(id)
            } else {
                document.getElementById(`cart-item-quantity-button-${id}`).innerHTML = 'ERROR'
            }

            setTimeout(() => {
                document.getElementById(`cart-item-quantity-button-${id}`).classList.remove('done')
                document.getElementById(`cart-item-quantity-button-${id}`).innerHTML = 'UPDATE'
                setAllowSubmit(true)
            }, 1200)
        })
    }

    const removeItem = (id) => {
        setAllowSubmit(false)
        fetch(`${process.env.REACT_APP_API_URL}/user/${user.id}/cart/update`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            },
            body: JSON.stringify({
                productId: id,
                quantity: 0
            })
        })
        .then(res => res.json())
        .then(data => {    
            if(data.status === 200) {
                retrieveUserCart()
                updateUserInfo()
                setAllowSubmit(true)
            } else {
                document.getElementById(`cart-item-quantity-button-${id}`).classList.add('done')
                document.getElementById(`cart-item-quantity-button-${id}`).innerHTML = 'ERROR'

                setTimeout(() => {
                    document.getElementById(`cart-item-quantity-button-${id}`).classList.remove('done')
                    document.getElementById(`cart-item-quantity-button-${id}`).innerHTML = 'UPDATE'
                    setAllowSubmit(true)
                }, 1200)
            }
        })
    }

    const emptyCart = () => {
        setAllowSubmit(false)
        fetch(`${process.env.REACT_APP_API_URL}/user/${user.id}/cart/empty`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            },
        })
        .then(res => res.json())
        .then(data => {    
            if(data.status === 200) {
                retrieveUserCart()
                updateUserInfo()
            }
            setAllowSubmit(true)
        })
    }

    const createCartData = async (newCart, render) => {
        const fetchedCart = await fetchCartList(newCart)
        setTotal(fetchedCart.reduce((total, item) => { 
            if (item.isActive === 'true'){
                return total + item.total 
            }
            return total
        }, 0))
        if(render === false) return

        const tempCart = fetchedCart.map(product => {
            return(
                <div className='cart-item' key={product.id}>
                    <Link as={NavLink} to={`/products/${product.handle}`} className='cart-item-image-wrapper'>
                        <img className='cart-item-image' src={product.image} alt={product.name} />
                    </Link>
                    <div className='cart-item-info'>
                        <p className='cart-item-name'>{product.name}</p>
                        <p className='cart-item-description'>{product.description}</p>
                        <input id={`cart-item-individual-price-${product.id}`} type="hidden" value={product.price} />
                        <p id={`cart-item-price-${product.id}`} className='cart-item-price'>USD ${product.total.toFixed(2)}</p>
                        <div className='cart-item-input s-input'>
                            <input 
                                id={`cart-item-quantity-${product.id}`}
                                className='cart-item-input-field' type='number' defaultValue={product.quantity}
                                onChange={e => updateQuantity(product.id, e.target.value)}
                                disabled={!allowSubmit}
                            />

                            <button 
                                id={`cart-item-quantity-button-${product.id}`}
                                className='cart-item-input-button s-button' 
                                onClick={() => setQuantity(product.id)}
                                disabled={!allowSubmit}
                            >
                                UPDATE
                            </button>
                        </div>
                        <div className='cart-item-remove-wrapper'>
                            {
                                product.isActive === 'false' &&
                                <p className='cart-item-remove-message'>
                                    item unavailable. will be removed on checkout
                                </p>
                            }
                            <p className='cart-item-remove' onClick={() => removeItem(product.id)}>remove</p>
                        </div>
                    </div>
                </div>
            )
        })
        setMyCart(tempCart);
    }

    const fieldsAsString = [
        'deliveryAddress',
    ]

    const submitForm = (e) => {
        e.preventDefault();
        fetch(`${process.env.REACT_APP_API_URL}/order/create`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            },
            body: JSON.stringify({
                userId: user.id,
                deliveryAddress
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data.hasOwnProperty('errors')) {
                for (const key in data.errors) {
                    if (fieldsAsString.includes(key)) {
                        document.getElementById(`${key}-error`).innerHTML = data.errors[key].message
                        document.getElementById(`${key}-error`).classList.remove('hidden')
                    } else {
                        document.getElementById(`${key}-error`).classList.add('hidden')
                    }
                }
            } else {
                for (const index in fieldsAsString) {
                    if (document.getElementById(`${fieldsAsString[index]}-error`)) {
                        document.getElementById(`${fieldsAsString[index]}-error`).classList.add('hidden')
                    }
                }

                if (data?.status === 201) {
                    updateUserInfo()
                    navigate(`/account#account-order-${data.order._id}`)

                }
            }
        })
    }

    const retrieveUserCart = (render=true) => {
        fetch(`${process.env.REACT_APP_API_URL}/user/${localStorage.getItem('id')}`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
          })
          .then(res => res.json())
          .then(data => {
              if(typeof data.user._id !== "undefined") {
                  createCartData(data.user.cart, render)
              }
          })
    }

    useEffect(() => {
        retrieveUserCart();
      }, []);

    return (
        (user.isAdmin === true || user.id === null) ?
            <Navigate to='/'/>
        :
            <>
                <div className='cart'>
                    <div className='cart-order'>
                        <div className='cart-header'>
                            <h1 className='cart-heading s-heading'>My Cart</h1>
                            {
                                (myCart.length > 0) &&
                                    <p className='cart-item-remove' onClick={() => emptyCart()}>empty cart</p>
                            }
                        </div>
                        <div className='cart-item-list'>
                            {myCart}
                            {
                                (myCart.length < 1) &&
                                <div className='cart-item-list-empty'>
                                    <p className='cart-item-list-empty__heading'>
                                        Cart is empty
                                    </p>
                                    <p className='cart-item-list-empty__subheading'>
                                        Click <Link as={NavLink} to='/collections/all'>here</Link> to check out all our products
                                    </p>
                                </div>
                            }
                        </div>
                    </div>
                    <form className='cart-summary' onSubmit={e => submitForm(e)} noValidate>
                        <div className='cart-header'>
                            <h1 className='cart-heading s-heading'>Order Summary</h1>
                        </div>
                        <div className='cart-table'>
                            <div className='cart-subtotal'>
                                <p className='cart-subtotal-label'>subtotal</p>
                                <p className='cart-subtotal-value'>USD ${total.toFixed(2)}</p>
                            </div>
                            <div className='cart-shipping'>
                                <p className='cart-shipping-label'>shipping</p>
                                <p className='cart-shipping-value'>Free</p>
                            </div>
                            <div className='cart-total'>
                                <p className='cart-total-label'>total</p>
                                <p className='cart-total-value'>USD ${total.toFixed(2)}</p>
                            </div>
                        </div>
                        {
                            (myCart.length > 0) &&
                            <>
                                <div className='s-input cart-delivery-address'>
                                    <label htmlFor="deliveryAddress">Delivery Address</label>
                                    <input
                                        id="deliveryAddress"
                                        name="deliveryAddress"
                                        type="text"
                                        value={deliveryAddress}
                                        onChange= {e => setDeliveryAddress(e.target.value)}
                                        disabled={!allowSubmit}
                                        required
                                    />
                                    <div id="deliveryAddress-error" className='s-input-error hidden'>
                                        Please enter a delivery address
                                    </div>
                                </div>
                                <div className='s-input cart-payment'>
                                    <label htmlFor="paymentMethod">Payment Method</label>
                                    <input
                                        id="paymentMethod"
                                        name="paymentMethod"
                                        type="text"
                                        value="Cash on Delivery"
                                        disabled
                                    />
                                </div>
                            </>
                        }
                        <div className='cart-cta'>
                            {
                                (myCart.length > 0) &&
                                    <button type='submit' className='s-button cart-submit' disabled={!allowSubmit}>Checkout</button>
                            }
                            <Link className='cart-return' as={NavLink} to={`/collections`}>Continue Shopping</Link>
                        </div>
                    </form>
                </div>
            </>
    )

}
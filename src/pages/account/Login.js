import { useState, useEffect, useContext } from 'react';

import { Navigate, useSearchParams, Link, NavLink } from 'react-router-dom';
import UserContext from '../../UserContext';

export default function Login(props) {

    const {user, setUser} = useContext(UserContext);

    const [searchParams] = useSearchParams();
    const [newUser, setNewUser] = useState(false);

    const [email, setEmail] = useState("");
    const [password1, setPassword1] = useState("");
    
    const fields = [
        email,
        password1,
    ]

    const fieldsAsString = [
        'email',
        'password1',
    ]

    function loginUser(e) {
        e.preventDefault()

        let allowSubmit = true;

        for(let index = 0; index < fields.length; index++) {
            if(fields[index] === '') {
                document.getElementById(`${fieldsAsString[index]}-error`).classList.remove('hidden')
                allowSubmit = false
            } else {
                document.getElementById(`${fieldsAsString[index]}-error`).classList.add('hidden')
            }
        }

        if(allowSubmit) {
            document.getElementById(`login-error`).classList.add('hidden')
            fetch(`${process.env.REACT_APP_API_URL}/user/login`, {
                method: 'POST',
                headers: {
                    'Content-type':'application/json'
                },
                body: JSON.stringify({
                    email: email,
                    password: password1
                })
            })
            .then(res => res.json())
            .then(data => {
                setPassword1("");
                if(data.hasOwnProperty('errors')) {
                    document.getElementById(`login-error`).classList.add('hidden')
                    for (const key in data.errors) {
                        if (fieldsAsString.includes(key)) {
                            document.getElementById(`${key}-error`).innerHTML = data.errors[key].message
                            document.getElementById(`${key}-error`).classList.remove('hidden')
                        } else {
                            document.getElementById(`${key}-error`).classList.add('hidden')
                        }
                    }
                } else {
                    for (const index in fieldsAsString) {
                        if (document.getElementById(`${fieldsAsString[index]}-error`)) {
                            document.getElementById(`${fieldsAsString[index]}-error`).classList.add('hidden')
                        }
                    }

                    if (data.message) {
                        document.getElementById(`login-error`).innerHTML = data.message
                        document.getElementById(`login-error`).classList.remove('hidden')
                    } else {
                        document.getElementById(`login-error`).classList.add('hidden')
                    }

                    if (data?.status === 200) {
                        localStorage.setItem('token', data.access);
                        retrieveUserDetails(data.access, data.id);
                    }
                }
            })
        } else {
            document.getElementById(`login-error`).classList.remove('hidden')
        }
    }

    const retrieveUserDetails = (token, id) => {
        fetch(`${process.env.REACT_APP_API_URL}/user/${id}`, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${token}`,
            },
        })
        .then(res => res.json())
        .then(data => {
            localStorage.setItem('id', data.user._id)

            const timestamp = Date.parse(data.user.createdOn)
            const date = new Date(timestamp)
            const joinDate = `${date.toLocaleString('en-US', { month: 'long' })} ${date.getDate()}, ${date.getFullYear()} ${String(date.getHours()).padStart(2, '0')}:${String(date.getMinutes()).padStart(2, '0')}`

            setUser({
                id: data.user._id,
                firstname: data.user.firstname,
                lastname: data.user.lastname,
                email: data.user.email,
                mobile: data.user.mobile,
                productsBought: data.user.productsBought,
                cartTotal: data.user.cart.reduce((total, current) => total + current.quantity, 0),
                isAdmin: data.user.isAdmin,
                joinDate
            })
        });
    }

    useEffect(() => {
        if(searchParams.get("registration")) {
            setNewUser(true)
        }
    }, [searchParams]);

    return (
        (user.id !== null) ?
            <Navigate to='/'/>
        :
            <>
                <div className='login-form'>
                    {
                        newUser === true ?
                            <p className='login-new'>
                                Successfully registered. Login with your new account
                            </p>
                        
                        :
                        <></>
                    }
                    <h1 className='s-heading'>Login</h1>
                    <div id='login-error' className='s-form-error hidden'>Form contains errors</div>
                    <form onSubmit={(e) => loginUser(e)} autoComplete='off' noValidate>
                        <div className='s-input'>
                            <label htmlFor="email">Email</label>
                            <input
                                id="email"
                                name="email"
                                type="email"
                                value={email}
                                onChange= {e => setEmail(e.target.value)}
                                required
                            />
                            <div id="email-error" className='s-input-error hidden'>
                                Please enter your Email
                            </div>
                        </div>

                        <div className='s-input'>
                            <label htmlFor="password1">Password</label>
                            <input
                                id="password1"
                                name="password1"
                                type="password"
                                value={password1}
                                onChange= {e => setPassword1(e.target.value)}
                                required
                            />
                            <div id="password1-error" className='s-input-error hidden'>
                                Please enter your Password
                            </div>
                        </div>

                        <button type="submit" id="submitButton" className='s-button'>Sign In</button>
                    </form>

                    <div className='account-redirect'>
                        <Link as={NavLink} to="/account/register">Create Account</Link>
                        <Link as={NavLink} to="/">Return to Store</Link>
                    </div>
                </div>
            </>
    )

}
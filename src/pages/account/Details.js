/* eslint-disable react-hooks/exhaustive-deps */
import { useState, useEffect, useContext } from 'react';
import { Navigate, Link, NavLink } from 'react-router-dom';
import UserContext from '../../UserContext';

export default function Details(props) {

    const { user } = useContext(UserContext);
    const [ myOrders, setMyOrders ] = useState([]);
    const [ myOrdersCount, setMyOrdersCount ] = useState('');

    const createOrderItem = (products) => {
        const tempProducts = products.map(product => {
            return(
                <div className='account-order-item' key={product._id}>
                    <div className='account-order-item-image-wrapper'>
                        <img className='account-order-item-image' src={product.image} alt=''/>
                    </div>
                    <div className='account-order-item-info'>
                        <p className='account-order-item-name'>{product.name} ({product.quantity})</p>
                        <p className='account-order-item-price'>USD ${ (product.price * product.quantity).toFixed(2) }</p>
                    </div>
                </div>
            )
        })
        return tempProducts
    }

    const createOrdersData = (userOrders) => {
        const tempOrders = userOrders.map( (order) => {
            const orderItems = createOrderItem(order.products)
            const timestamp = Date.parse(order.createdOn)
            const date = new Date(timestamp)
            const formattedDate = `${date.toLocaleString('en-US', { month: 'long' })} ${date.getDate()}, ${date.getFullYear()} ${String(date.getHours()).padStart(2, '0')}:${String(date.getMinutes()).padStart(2, '0')}`
            return (
                <div id={`account-order-${order._id}`} className='account-order' key={order._id}>
                    <div className='account-order-header'>
                        <p className='s-heading account-order-heading'>Order ID: { order._id }</p>
                    </div>
                    <div className='account-order-info'>
                        <div className='account-order-item-info-group'>
                            <div className='s-input account-order-item-payment'>
                                <label htmlFor='account-order-item-payment'>PAYMENT METHOD</label>
                                <input id='account-order-item-payment' value={ order.paymentMethod } disabled />
                            </div>
                            <div className='s-input account-order-item-date'>
                                <label htmlFor='account-order-item-date'>ORDER DATE</label>
                                <input id='account-order-item-date' value={ formattedDate } disabled />
                            </div>
                        </div>
                        <div className='s-input account-order-item-address'>
                            <label htmlFor="account-order-item-address">DELIVERY ADDRESS</label>
                            <input id='account-order-item-address' value={order.deliveryAddress} disabled />
                        </div>
                    </div>
                    <div className='account-order-list'>
                        <div className='account-order-item-header'>
                            <p className='account-order-item-total'>TOTAL</p>
                            <p className='account-order-item-total-price'>USD ${order.totalPrice.toFixed(2)}</p>
                        </div>
                        { orderItems }
                    </div>
                </div>
            )
        })
        setMyOrders(tempOrders);
    }

    const retrieveUserOrders = () => {
        fetch(`${process.env.REACT_APP_API_URL}/order/user/${localStorage.getItem('id')}`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
          })
          .then(res => res.json())
          .then(data => {
              setMyOrdersCount(0)

              if(data.count > 0) {
                createOrdersData(data.orders)
                setMyOrdersCount(data.orders.length)
              }
          })
    }

    useEffect(() => {
        retrieveUserOrders();
    }, []);

    return (
        (user.isAdmin === true || user.id === null) ?
            <Navigate to='/'/>
        :
            <>
                <div className='account'>
                    <div className='account-details'>
                        <div className='account-header'>
                            <h1 className='account-heading s-heading'>Account Details</h1>
                        </div>
                        <div className='account-details-content'>
                            <div className='account-details-wrapper s-input'>
                                <label htmlFor='account-details-name'>Name</label>
                                <input 
                                    id='account-details-name'
                                    name='account-details-name'
                                    className='account-details-name' 
                                    value={ user.firstname + ' ' + user.lastname }
                                    disabled
                                />
                            </div>
                            <div className='account-details-wrapper s-input'>
                                <label htmlFor='account-details-email'>Email</label>
                                <input 
                                    id='account-details-email'
                                    name='account-details-email'
                                    className='account-details-email' 
                                    value={ user.email }
                                    disabled
                                />
                            </div>
                            <div className='account-details-wrapper s-input'>
                                <label htmlFor='account-details-mobile'>Mobile</label>
                                <input 
                                    id='account-details-mobile'
                                    name='account-details-mobile'
                                    className='account-details-mobile' 
                                    value={ user.mobile }
                                    disabled
                                />
                            </div>
                            <div className='account-details-wrapper s-input'>
                                <label htmlFor='account-details-joined'>Joined</label>
                                <input 
                                    id='account-details-joined'
                                    name='account-details-joined'
                                    className='account-details-joined' 
                                    value={ user.joinDate }
                                    disabled
                                />
                            </div>
                        </div>
                    </div>
                    <div className='account-orders'>
                        <div className='account-header'>
                            <h1 className='account-heading s-heading'>Orders ({myOrdersCount})</h1>
                        </div>
                        <div className='account-orders-list'>
                            {myOrders}
                            {
                                (myOrders.length < 1) &&
                                <div className='account-order-list-empty'>
                                    <p className='account-order-list-empty__heading'>
                                        No orders
                                    </p>
                                    <p className='account-order-list-empty__subheading'>
                                        Click <Link as={NavLink} to='/collections/all'>here</Link> to check out all our products
                                    </p>
                                </div>
                            }
                        </div>
                    </div>
                </div>
            </>
    )

}
import {useState, useContext} from 'react';
import { Navigate, useNavigate, NavLink, Link } from 'react-router-dom';
import UserContext from '../../UserContext';

export default function Register() {

    const { user } = useContext(UserContext);
    const navigate = useNavigate()

    const [firstname, setFirstname] = useState("");
    const [lastname, setLastname] = useState("");
    const [email, setEmail] = useState("");
    const [mobile, setMobile] = useState("");

    const [password1, setPassword1] = useState("");
    const [password2, setPassword2] = useState("");
    
    const fields = [
        firstname,
        lastname,
        email,
        mobile,
        password1,
        password2,
    ]

    const fieldsAsString = [
        'firstname',
        'lastname',
        'email',
        'mobile',
        'password1',
        'password2',
    ]

    function registerUser(e) {
        e.preventDefault()

        let allowSubmit = true;

        for(let index = 0; index < fields.length; index++) {
            if(fields[index] === '') {
                document.getElementById(`${fieldsAsString[index]}-error`).classList.remove('hidden')
                allowSubmit = false
            } else {
                document.getElementById(`${fieldsAsString[index]}-error`).classList.add('hidden')
            }
        }

        if(password1 !== password2) {
            document.getElementById(`password-error`).classList.remove('hidden')
            allowSubmit = false
        } else {
            document.getElementById(`password-error`).classList.add('hidden')
        }

        if(allowSubmit) {
            document.getElementById(`register-error`).classList.add('hidden')
            fetch(`${process.env.REACT_APP_API_URL}/user/register`, {
                method: 'POST',
                headers: {
                    'Content-type':'application/json'
                },
                body: JSON.stringify({
                    firstname: firstname,
                    lastname: lastname,
                    email: email,
                    mobile: mobile,
                    password: password1
                })
            })
            .then(res => res.json())
            .then(data => {
                if(data.hasOwnProperty('errors')) {
                    document.getElementById(`register-error`).classList.add('hidden')
                    for (const key in data.errors) {
                        if (fieldsAsString.includes(key)) {
                            document.getElementById(`${key}-error`).innerHTML = data.errors[key].message
                            document.getElementById(`${key}-error`).classList.remove('hidden')
                        } else {
                            document.getElementById(`${key}-error`).classList.add('hidden')
                        }
                    }
                } else {
                    for (const index in fieldsAsString) {
                        if (document.getElementById(`${fieldsAsString[index]}-error`)) {
                            document.getElementById(`${fieldsAsString[index]}-error`).classList.add('hidden')
                        }
                    }

                    if (data.message) {
                        document.getElementById(`register-error`).innerHTML = data.message
                        document.getElementById(`register-error`).classList.remove('hidden')
                    } else {
                        document.getElementById(`register-error`).classList.add('hidden')
                    }

                    if (data?.status === 201) {
                        navigate('/account/login?registration=success');
                    }
                }
            })
        } else {
            document.getElementById(`register-error`).classList.remove('hidden')
        }
    }

    return (
        (user.id !== null) ?
            <Navigate to='/'/>
        :
        <>
            <div className='register-form'>
                <h1 className='s-heading'>Register</h1>
                <div id='register-error' className='s-form-error hidden'>Form contains errors</div>
                <form onSubmit={(e) => registerUser(e)} autoComplete='off'>
                    <fieldset className='s-form__fieldset'>
                        <div className='s-input'>
                            <label htmlFor="firstname">First Name</label>
                            <input
                                id="firstname"
                                name="firstname"
                                type="text"
                                value={firstname}
                                onChange= {e => setFirstname(e.target.value)}
                                required
                            />
                            <div id="firstname-error" className='s-input-error hidden'>
                                Please enter your First Name
                            </div>
                        </div>

                        <div className='s-input'>
                            <label htmlFor="lastname">Last Name</label>
                            <input
                                id="lastname"
                                name="lastname"
                                type="text" 
                                value={lastname}
                                onChange= {e => setLastname(e.target.value)}
                                required
                            />
                            <div id="lastname-error" className='s-input-error hidden'>
                                Please enter your Last Name
                            </div>
                        </div>
                    </fieldset>

                    <div className='s-input'>
                        <label htmlFor="email">Email Address</label>
                        <input
                            id="email"
                            name="email"
                            type="email"
                            value={email}
                            onChange= {e => setEmail(e.target.value)}
                            required
                        />
                        <div id="email-error" className='s-input-error hidden'>
                            Please enter your Email Address
                        </div>
                    </div>

                    <div className='s-input'>
                        <label htmlFor="mobile">Mobile</label>
                        <input
                            id="mobile"
                            name="mobile"
                            type="text" 
                            value={mobile}
                            onChange= {e => setMobile(e.target.value)}
                            required
                        />
                        <div id="mobile-error" className='s-input-error hidden'>
                            Please enter your Mobile
                        </div>
                    </div>

                    <div className='s-input'>
                        <label htmlFor="password1">Password</label>
                        <input
                            id="password1"
                            name="password1"
                            type="password"
                            value={password1}
                            onChange= {e => setPassword1(e.target.value)}
                            required
                        />
                        <div id="password1-error" className='s-input-error hidden'>
                            Please enter your Password
                        </div>
                    </div>

                    <div className='s-input'>
                        <label htmlFor="password2">Confirm Password</label>
                        <input
                            id="password2"
                            name="password2"
                            type="password"
                            value={password2}
                            onChange= {e => setPassword2(e.target.value)}
                            required
                        />
                        <div id="password2-error" className='s-input-error hidden'>
                            Please enter your Password again
                        </div>
                    </div>

                    <div id="password-error" className='s-input-error hidden'>
                        Passwords must be the same
                    </div>

                    <p className='s-fine-print'>By signing up you agree to our Terms of Service and Privacy Policy</p>
                    <button type="submit" id="submitButton" className='s-button'>Sign Up</button>
                </form>
                <div className='account-redirect'>
                    <Link as={NavLink} to="/account/login">Login</Link>
                    <Link as={NavLink} to="/">Return to Store</Link>
                </div>
            </div>
        </>
    )
}
